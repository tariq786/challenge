import urllib
import webbrowser as wb

from xml.etree.ElementTree import parse
opening_part = "https://maps.google.com/maps?"

candidates = ['4337', '4345', '4353', '4052']
friends_lat = 41.980262
friends_long = -87.668452

def distance (lat1, lat2):
    'Return distance in miles between two lats'
    return 69*abs(lat1-lat2)

def monitor():
    u = urllib.urlopen('http://ctabustracker.com/bustime/map/getBusesForRoute.jsp?route=22')
    doc = parse(u)
    for bus in doc.findall('bus'):
        busid = bus.findtext('id')
        if busid in candidates:
            lat = float(bus.findtext('lat'))
            dis = distance(lat, friends_lat)
            print busid, dis, 'miles'
            place = "q=%s,%s" %(str(friends_lat),str(friends_long))
            print place
            wb.open(opening_part+place)
    print '-'*10

import time
while True:
    monitor()
    time.sleep(10)
